def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

   pipeline {
       agent any
       stages {
           stage('Git Checkout') {
               steps {
                   echo "Pulling " + config.BRANCH
                   checkout([
                       $class: 'GitSCM', 
                       branches: [[name: config.BRANCH]], 
                       extensions: [], 
                       userRemoteConfigs: [[credentialsId: 'gitcredentials', 
                       url: 'http://gitlab.example.com/agdias/rabbitmq.git']]])
               }

           }  
           stage('Build') {
               steps {
                  sh "/var/jenkins_home/apache-maven-3.8.5/bin/mvn -DskipTests  -f stock-exchange/pom.xml  -f  clean  compile package"
               }
           }
           stage('gitlab') {
              steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'build', state: 'pending'
               updateGitlabCommitStatus name: 'build', state: 'success'
            }
       }
   }
    
}
